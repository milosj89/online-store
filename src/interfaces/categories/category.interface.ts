/**
 * Category interface
 *
 * @author Milos Jovanovic
 */

export class CategoryInterface {
    CategoryID: number;
    CategoryName: string;
    Description: string;
    Picture: string;
}
