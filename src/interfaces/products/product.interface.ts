/**
 * Product interface
 *
 * @author Milos Jovanovic
 */

export interface ProductInterface {
    CategoryID: number;
    CategoryName: string;
    Discontinued: boolean;
    ProductID: number;
    ProductName: string;
    Quantity?: number;
    QuantityPerUnit: string;
    ReorderLevel: number;
    SupplierID: number;
    UnitPrice: string;
    UnitsInStock: number;
    UnitsOnOrder: number;
}
