/**
 * Api service
 *
 * Contains requests to server
 *
 * @author Milos Jovanovic
 */

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

// 3RD PARTY
import {Observable} from 'rxjs';

// CONST
import {ENV} from '../ENV';

@Injectable()
export class ApiService {

    constructor(private http: HttpClient) {
    }

    public getData(param: string): Observable<any> {
        return this.http.get(`${ENV.BASE_URl}${param}?$format=json`);
    }

    public getApp(): Observable<any> {
        return this.http.get('http://localhost:3000/api/categories');
    }
}

