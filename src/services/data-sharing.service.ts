/**
 * Data sharing service
 *
 * @author Milos Jovanovic
 */

import {Injectable} from '@angular/core';

// 3RD PARTY
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class DataSharingService {

    public counter: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    public modal: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    public products: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
}
