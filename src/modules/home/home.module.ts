/**
 * Home module
 *
 * @author Milos Jovanovic
 */

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

// COMPONENTS
import {CartComponent} from './components/cart/cart.component';
import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {ProductDetailsComponent} from './components/product-details/product-details.component';
import {ProductItemComponent} from './components/product-item/product-item.component';
import {ProductsComponent} from './components/products/products.component';

// PAGES
import {HomePage} from './pages/home/home.page';

// ANGULAR MATERIAL
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {MatAutocompleteModule, MatButtonModule, MatCardModule, MatInputModule} from '@angular/material';

const routes: Routes = [
    {path: 'home', component: HomePage, children: [
        {path: '', component: ProductsComponent},
        {path: 'product-details/:productId', component: ProductDetailsComponent}
        ]},
    {path: '**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
    declarations: [
        CartComponent,
        FooterComponent,
        HeaderComponent,
        ProductDetailsComponent,
        ProductItemComponent,
        ProductsComponent,
        HomePage
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatTabsModule,
        MatIconModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
    ]
})
export class HomeModule {
}
