/**
 * Product details component
 *
 * Contains details of product
 *
 * @author Milos Jovanovic
 */

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

// INTERFACES
import {ProductInterface} from '../../../../interfaces/products/product.interface';

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
    /**
     * Id of product which user is choose
     */
    productId: number;

    /**
     * Product which user is choose
     */
    product: ProductInterface;

    constructor(private activatedRoute: ActivatedRoute) {
    }

    /**
     * Angular lifecycle hook
     *
     * Initialize product which user is choose
     */
    public ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(param => {
            this.productId = +(param.get('productId'));
        });
        this.getProduct();
    }

    /**
     * Get product from local storage which user is choose by id from url address
     */
    public getProduct(): void {
        let products: ProductInterface[];
        products = JSON.parse(localStorage.getItem('allProducts'));
        this.product = products.find(this.getProductById.bind(this));
    }

    /**
     * Return product which has same id as id from url address
     *
     * @param {ProductInterface} product
     */
    public getProductById(product: ProductInterface): ProductInterface {
        if (product.ProductID === this.productId) {
            return product;
        }
    }
}
