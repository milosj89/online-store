/**
 * Footer component
 *
 * Contains structure and methods for footer
 *
 * @author Milos Jovanovic
 */

import {Component} from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

}
