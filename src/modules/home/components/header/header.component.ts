/**
 * Header component
 *
 * Contains structure and methods for header
 *
 * @author Milos Jovanovic
 */
import {Component, OnInit} from '@angular/core';

// SERVICES
import {DataSharingService} from '../../../../services/data-sharing.service';

// INTERFACES
import {ProductInterface} from '../../../../interfaces/products/product.interface';

// 3RD PARTY
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    /**
     * Contains names of all products
     */
    public allProductNames: string[];

    /**
     * @type {FormControl} control
     * Form control
     */
    public control: FormControl = new FormControl();

    /**
     * Contains filtered products when user start typing in search bar
     */
    public filteredProducts: Observable<string[]>;

    /**
     * Number of products that in cart
     */
    public numberProductsInCart: number;

    /**
     * Determine whether cart open or close
     */
    public openModal: boolean;

    /**
     * Products that in cart
     */
    public products: ProductInterface[] = [];

    /**
     * Show total for payment in cart
     */
    public total = 0;

    constructor(private dataSharingService: DataSharingService) {
    }

    /**
     * Angular lifecycle hook
     *
     * Initialize of number of products in cart
     */
    public ngOnInit(): void {
        this.dataSharingService.counter.subscribe({
            error: console.log,
            next: this.showCountOfProducts.bind(this)
        });
        this.openModal = false;
        this.dataSharingService.products.subscribe({
            error: console.log,
            next: product => this.allProductNames = product.map(prod => prod.ProductName)
        });
        this.filteredProducts = this.control.valueChanges.pipe(
            startWith(''),
            map(value => this.filter(value))
        );
    }

    /**
     * Change value of property modal for open cart
     *
     * @param {boolean} modal
     */
    public changeValueModal(modal: boolean): void {
        this.openModal = modal;
    }

    /**
     * Return array with product names when user start typing in searchbar
     *
     * @param {String} value
     * @return string[]
     */
    private filter(value: string): string[] {
        const filterValue = this.normalizeValue(value);
        return this.allProductNames.filter(street => this.normalizeValue(street).includes(filterValue));
    }

    /**
     * Return string that user entered in searchbar
     *
     * @param {String} value
     * @return string
     */
    public normalizeValue(value: string): string {
        return value.toLowerCase().replace(/\s/g, '');
    }

    /**
     * Open modal with products in cart
     */
    public openCart(): void {
        if (localStorage.getItem('productsInCart')) {
            this.products = JSON.parse(localStorage.getItem('productsInCart'));
        }
        this.showProductsInCart(this.products);

        this.dataSharingService.modal.next(true);
        this.dataSharingService.modal.subscribe({
            error: console.log,
            next: this.changeValueModal.bind(this)
        });
    }

    /**
     * Display number of quantity products in cart
     *
     * @param {number} count
     */
    public showCountOfProducts(count: number): void {
        this.numberProductsInCart = count;
    }

    /**
     * Show products when user open cart
     *
     * @param {ProductInterface[]} products
     */
    public showProductsInCart(products: ProductInterface[]): void {
        this.total = 0;
        products.forEach(product => {
            this.total += +product.UnitPrice * product.Quantity;
        });
    }
}
