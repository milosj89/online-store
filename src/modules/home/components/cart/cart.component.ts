/**
 * Cart component
 *
 * Contains products in cart
 *
 * @author Milos Jovanovic
 */

import {Component, Input} from '@angular/core';

// SERVICES
import {DataSharingService} from '../../../../services/data-sharing.service';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})

export class CartComponent {
    /**
     * Determine whether cart open or close
     */
    @Input() public modal: boolean;

    /**
     * Contains products that in cart
     */
    @Input() public productsInCart: any[] = [];

    /**
     * Show total for payment in cart
     */
    @Input() public totalForPayment: number;

    constructor(private dataSharingService: DataSharingService) {
    }

    public checkout() {
        this.dataSharingService.counter.next(0);
        this.closeCart();
        localStorage.removeItem('productsInCart');
        localStorage.setItem('isCheckout', JSON.stringify(true));
    }


    /**
     * Close cart with products
     */
    public closeCart(): void {
        this.dataSharingService.modal.next(false);
    }

    /**
     * Delete products from cart
     *
     * @param {Event} event
     */
    public deleteProduct(event: any): void {
        const btnId = +event.target.id;
        this.productsInCart = this.productsInCart.filter(product => product.ProductID !== btnId);
        localStorage.setItem('productsInCart', JSON.stringify(this.productsInCart));
        this.totalForPayment = 0;
        this.productsInCart.forEach(product => this.totalForPayment += +product.UnitPrice * product.Quantity);
        let count = 0;
        this.productsInCart.forEach(product => count += product.Quantity);
        this.dataSharingService.counter.next(count);
    }
}
