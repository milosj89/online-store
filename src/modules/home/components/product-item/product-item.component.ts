/**
 * Products component
 *
 * Contains methods and template about product-item
 *
 * @author Milos Jovanovic
 */

import {Component, ElementRef, Input, ViewChild} from '@angular/core';

// SERVICES
import {DataSharingService} from '../../../../services/data-sharing.service';

// INTERFACES
import {ProductInterface} from '../../../../interfaces/products/product.interface';

@Component({
    selector: 'app-product-item',
    templateUrl: './product-item.component.html',
    styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent {
    /**
     * Product that the user chooses
     */
    @Input() public chooseProduct: ProductInterface;

    /**
     * Number of products in cart
     */
    public count: number;

    /**
     * Products that the user added in cart
     */
    public productsInCart: ProductInterface[] = [];

    /**
     * Number of products that user adds in cart
     */
    public quantity = 1;

    /**
     * Quantity of product from input field
     */
    @ViewChild('inputQuantity', {static: false}) quantityProducts: ElementRef;

    /**
     * Contains products which display
     * @type {object[]}
     */
    @Input() public showProducts: ProductInterface[] = [];

    constructor(private dataSharingService: DataSharingService) {
    }

    /**
     * Method for add products to cart
     */
    public addToCart(): void {
        if (localStorage.getItem('isCheckout')) {
            this.productsInCart = [];
            localStorage.removeItem('isCheckout');
        }

        if (!(localStorage.getItem('productsInCart'))) {
            this.chooseProduct.Quantity = +this.quantityProducts.nativeElement.value;
            this.productsInCart.push(this.chooseProduct);
        } else {
            this.productsInCart = JSON.parse(localStorage.getItem('productsInCart'));

            let productExists = false;

            for (const product of this.productsInCart) {
                if (product.ProductID === this.chooseProduct.ProductID) {
                    product.Quantity += +this.quantityProducts.nativeElement.value;
                    productExists = true;
                    break;
                }
            }

            if (!productExists) {
                this.chooseProduct.Quantity = +this.quantityProducts.nativeElement.value;
                this.productsInCart.push(this.chooseProduct);
            }
        }
        localStorage.setItem('productsInCart', JSON.stringify(this.productsInCart));

        this.counterProductsInCart(this.productsInCart);
    }

    /**
     * Change number of products in cart when user added product in cart
     *
     * @param {ProductInterface} product
     */
    public changeCountProductInCart(product: ProductInterface) {
        this.count += product.Quantity;
        this.dataSharingService.counter.next(this.count);
    }

    /**
     * Counter number of products in the cart
     *
     * @param {ProductInterface} products
     */
    public counterProductsInCart(products: ProductInterface[]): void {
        this.count = 0;
        products.forEach(this.changeCountProductInCart.bind(this));
    }
}
