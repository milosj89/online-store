/**
 * Dashboard component
 *
 * Contains structure and methods for products
 *
 * @author Milos Jovanovic
 */

import {Component, OnInit} from '@angular/core';

// SERVICES
import {ApiService} from '../../../../services/api.service';

// 3RD PARTY
import {PartialObserver} from 'rxjs';

// INTERFACES
import {ProductInterface} from '../../../../interfaces/products/product.interface';
import {CategoryInterface} from '../../../../interfaces/categories/category.interface';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['/products.component.scss']
})
export class ProductsComponent implements OnInit {

    /**
     * Contains products after click on tab
     */
    public actualProducts: ProductInterface[];

    /**
     * Contains categories from server
     */
    public categories: CategoryInterface[];

    /**
     * Contains names of groups product-item
     */
    public items: string[] = ['All', 'Action', 'Top'];

    /**
     * Contains products from server
     */
    public products: ProductInterface[] = [];

    constructor(private apiService: ApiService) {
    }

    /**
     * Angular lifecycle hook
     * Initialize products when start application
     */
    public ngOnInit(): void {
        this.apiService.getData('Products').subscribe(this.getProductsObserver());
        this.apiService.getData('Categories').subscribe(this.getCategoriesObserver());
        this.apiService.getApp().subscribe(this.getAppObserver());
    }

    public getAppObserver(): PartialObserver<any> {
        return {
            error: console.log,
            next: value => console.log(value[0].name)
        };
    }
    /**
     * Round the decimal number of price
     *
     * @param {ProductInterface} product
     */
    public changeTypeOfPrice(product: ProductInterface): string {
        return product.UnitPrice = (+product.UnitPrice).toFixed(0);
    }

    /**
     * Determine which products are displayed depending on which tab is clicked
     *
     * @param event {Event}
     * @param products {ProductInterface[]}
     */
    public displayProducts(event: any, products: ProductInterface[]): void {
        switch (event.tab.textLabel.toLowerCase()) {
            case 'all':
                this.actualProducts = products;
                break;
            case 'action':
                this.actualProducts = products.filter(this.filterProductsByCategoriesID.bind(this, 1));
                break;
            case 'top':
                this.actualProducts = products.filter(this.filterProductsByCategoriesID.bind(this, 2));
                break;
        }
    }

    /**
     * Get category name by category ID
     *
     * @param categories {object}
     */
    public getCategories(categories: any): void {
        this.categories = categories.value;
        this.products.forEach(this.getCategoryName.bind(this));
    }

    /**
     * Get products from server
     */
    public getCategoriesObserver(): PartialObserver<any> {
        return {
            error: console.log,
            next: this.getCategories.bind(this)
        };
    }

    /**
     * Get category names
     *
     * @param {ProductInterface} product
     */
    public getCategoryName(product: ProductInterface): void {
        this.categories.forEach(this.categoryNameByCategoryID.bind(this, product));
    }

    /**
     * Determines category name by category ID
     *
     * @param {ProductInterface} product
     * @param {CategoryInterface} category
     */
    public categoryNameByCategoryID(product: ProductInterface, category: CategoryInterface) {
        if (product.CategoryID === category.CategoryID) {
            product.CategoryName = category.CategoryName;
        }
    }

    /**
     * Assign products from server to new array
     *
     * @param products {object[]}
     */
    public getProducts(products: any): void {
        this.products = products.value;
        this.products.map(this.changeTypeOfPrice.bind(this));
        localStorage.setItem('allProducts', JSON.stringify(this.products));
        this.actualProducts = this.products;
    }

    /**
     * Get products from server
     */
    public getProductsObserver(): PartialObserver<any> {
        return {
            error: console.log,
            next: this.getProducts.bind(this)
        };
    }

    /**
     * Filter products by category id
     *
     * @param {number}id
     * @param {ProductInterface}product
     */
    public filterProductsByCategoriesID(id: number, product: ProductInterface): boolean {
        return product.CategoryID === id;
    }
}
