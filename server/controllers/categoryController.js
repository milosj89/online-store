const mongoJs = require('mongojs');
const db = mongoJs('onlineStore', ['categories']);

const categoryController = (req, res) => {
    db.categories.find({}, (err, categories) => {
        res.send(categories);
    })
}

module.exports = categoryController;
