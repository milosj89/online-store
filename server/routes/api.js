const express = require('express');
const router = express.Router();

router.get('/categories', require('../controllers/categoryController'));

module.exports = router;
